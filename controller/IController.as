package engine.controller 
{
	
	/**
	 * ...
	 * @author Daniel McMillon
	 */
	public interface IController 
	{
		function setup():void;
		function dispose():void;
	}
	
}