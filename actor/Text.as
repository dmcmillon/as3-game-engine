package engine.actor 
{
	import engine.display.IDisplayable;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	/**
	 * ...
	 * @author Daniel McMillon
	 */
	public class Text implements IDisplayable
	{
		
		public function Text() 
		{
			
		}
		
		public function get ImageMatrix():Matrix
		{
			
		}
		
		public function get Image():DisplayObject
		{
			
		}
		
		public function set Image(value:DisplayObject):void
		{
			
		}
		
		public function set IsVisible(visibility:Boolean):void
		{
			
		}
		
		public function get IsVisible():Boolean
		{
			
		}
	}

}