package engine.display 
{
	import flash.display.MovieClip;
	
	/**
	 * ...
	 * @author Daniel McMillon
	 */
	public interface IMovieClip 
	{
		function get Image():MovieClip;
	}
}